import { Kuzzle, WebSocket } from 'kuzzle-sdk';

const kuzzleOptions = {
  offlineMode: 'auto'
};
const webSocketOptions = {
  port: parseInt(process.env.REACT_APP_KUZZLE_BACKEND_PORT),
  autoReconnect: true,
  sslConnection: false,
  reconnectionDelay: 1000
};

export default new Kuzzle(new WebSocket(process.env.REACT_APP_KUZZLE_BACKEND_DOMAIN_NAME.toString(), webSocketOptions ), kuzzleOptions);
