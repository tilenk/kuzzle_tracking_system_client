import styled from "styled-components";

export const Accounts = styled.div`
    position: absolute;
    border: 1px solid black;
    top: 100px;
    z-index: 9999;
    width: 20%;
    background-color: white;
`
export const RowParent = styled.div`
    margin: 0.5rem;
    text-align: left;
`
export const RowChild = styled.div`
    display: inline-block;
    width: 90px;
    padding: 0rem 0.5rem;
    vertical-align: middle;
`

export const Button = styled.button`
    background-color: black;
    color: white;
    font-size: 20px;
    padding: 5px 50px;
    border-radius: 5px;
    margin: 10px 0px;
    cursor: pointer;
`;