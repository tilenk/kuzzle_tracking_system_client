import React, { useEffect, useState } from "react";
import kuzzle from "./services/kuzzle";
import 'react-dropdown/style.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import pathOr from 'ramda/src/pathOr'
import map from 'ramda/src/map';
import addIndex from 'ramda/src/addIndex';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';
import { ToggleButton } from "./components/atoms/toggleButton";
import { Accounts, RowParent, RowChild, Button } from "./styles";

let allMarkers = []
let allNotifications = 0

function App() {
  const [isSubscribed, setIsSubscribed] = useState({});
  const [isSubscribedRoom, setIsSubscribedRoom] = useState({});
  const [accounts, setAccounts] = useState([]);
  const [markers, setMarkers] = useState([]);
  const [countNotifications, setCountNotifications] = useState(0);
  const [showMarkers, setShowMarkers] = useState(true);

  let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
  });
  L.Marker.prototype.options.icon = DefaultIcon;

  const callback = async (notification) => {
    if (notification.scope === 'in') {
      allMarkers = [...allMarkers, {
        data: notification.result._source.data,
        position: notification.result._source.position
      }]
      setMarkers(allMarkers)
      allNotifications = allNotifications + 1
      setCountNotifications(allNotifications)
    }
  };

  const toggleSubscribe = async (accountId) => {
    const subscribeAction = !isSubscribed[accountId]
    setIsSubscribed({
      ...isSubscribed,
      [accountId]: subscribeAction
    })

    if (subscribeAction) {
      const options = { scope: 'in' };
      let roomId = await kuzzle.realtime.subscribe('system', 'events', {
        equals: {
          selector: `${accountId}true`
        }
      }, callback, options);
      console.log(`Account ${accountId} successfully subscribed to room: ${roomId}`)
      setIsSubscribedRoom({
        ...isSubscribedRoom,
        [accountId]: roomId
      })
    } else if (isSubscribedRoom[accountId]) {
      try {
        await kuzzle.realtime.unsubscribe(isSubscribedRoom[accountId]);
        console.log(`Unsubscribing from room id: ${isSubscribedRoom[accountId]}`)
        setIsSubscribedRoom({
          ...isSubscribedRoom,
          [accountId]: null
        })
      } catch (err) {
        console.error('Error occured while trying to unsubscribe from room', err)
      }
    }
  }

  useEffect(() => {
    (async () => {
      try {
        console.log("Connecting to kuzzle...")
        await kuzzle.connect();
        await kuzzle.auth.login('local', {
          username: process.env.REACT_APP_KUZZLE_BACKEND_ADMIN_USERNAME,
          password: process.env.REACT_APP_KUZZLE_BACKEND_ADMIN_PASSWORD
        });
        let accounts = await kuzzle.document.search(
          'system',
          'accounts',
          {
            query: {}
          }
        )
        setAccounts(pathOr([], ['hits'], accounts))
      } catch (err) {
        console.error('Error occured while trying to establish connection top kuzzle', err)
      }
    })()
  }, []);

  return (
    <div className="App">
      <MapContainer
        center={[46.056946, 14.505751]} zoom={5} scrollWheelZoom={false}
        style={{ width: '100%', height: '1200px' }}
      >
        <TileLayer
          attribution='&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
        {showMarkers && markers.map((marker, idx) =>
          <Marker key={`marker-${idx}`} position={marker.position}>
            <Popup>
              <span>{marker.data}</span>
            </Popup>
          </Marker>
        )}
      </MapContainer>
      <Accounts>
          <RowParent key='notificationInfo'>
            <RowChild>
              Notifications
            </RowChild>
            <RowChild>
              {countNotifications}
            </RowChild>
          </RowParent>
          <RowParent key='showMarkers'>
            <RowChild>
              Show markers
            </RowChild>
            <RowChild>
              <ToggleButton
                selected={showMarkers}
                toggleSelected={() => {
                  setShowMarkers(!showMarkers);
                }}
              />
            </RowChild>
          </RowParent>
          {addIndex(map)((account, index) => {
            return (
              <RowParent key={`${index}`}>
                <RowChild>
                  {account?._source?.name}
                </RowChild>
                <RowChild>
                  <Button onClick={() => toggleSubscribe(account?._id)}>
                    {isSubscribed[account?._id] ?
                      'Unsubscribe'
                      :
                      'Subscribe'
                    }
                  </Button>
                </RowChild>
              </RowParent>
            );
          })(accounts ?? [])}
      </Accounts>
    </div>
  );
}

export default App;
